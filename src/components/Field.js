import React, { Component } from 'react';
import PropTypes from 'prop-types';
import clone from 'lodash/clone';
import FormationSelector from './FormationSelector';
import Jersey from './Jersey';
import Modal from './Modal';

export const setPlayerRows = (formation) => {
    let formationIncludingGoalKeeper = `1-${formation}`;
    return formationIncludingGoalKeeper.split('-').map((numberOfPlayersInRow) => {
        let row = [];
        for (let i = parseInt(numberOfPlayersInRow, 10); i > 0; i--) {
            row.push(null);
        }
        return row;
    });
};

export const updatePlayerRow = ({ player, row, column }, playerRows) => {
    for (let i = 0; i < playerRows[row].length; i++) {
        if (playerRows[row][i] === player) {
            playerRows[row][i] = null;
        }
    }
    playerRows[row][column] = player;
    return playerRows;
};

class Field extends Component {
    static propTypes = {
        formations: PropTypes.array.isRequired,
        players: PropTypes.object.isRequired
    };

    state = {
        activeFormation: '',
        playerRows: [],
        modalPlayer: { show: false }
    };

    componentDidMount() {
        this.setState(() => ({
            activeFormation: this.props.formations[0]
        }));
    }

    componentDidUpdate(prevProps, prevState) {
        if (
            this.state.playerRows.length === 0 ||
            this.state.activeFormation !== prevState.activeFormation
        ) {
            this.setState((prevState) => ({
                playerRows: setPlayerRows(prevState.activeFormation)
            }));
        }
    }

    handleFormationChange = (activeFormation) => {
        this.setState(() => ({
            activeFormation
        }));
    };

    handlePlayerClick = ({ player, row, column }) => {
        this.setState((prevState, props) => ({
            modalPlayer: {
                show: true,
                row,
                column,
                position: Object.keys(props.players)[row]
            }
        }));
    };

    handlePlayerChange = (player) => {
        this.setState((prevState) => ({
            playerRows: updatePlayerRow(
                { player, row: prevState.modalPlayer.row, column: prevState.modalPlayer.column },
                clone(prevState.playerRows)
            ),
            modalPlayer: { show: false }
        }));
    };

    handleReset = () => {
        this.setState((prevState, props) => ({
            activeFormation: props.formations[0],
            playerRows: []
        }));
    };

    render() {
        return (
            <div className="field-container">
                <FormationSelector
                    formations={this.props.formations}
                    activeFormation={this.state.activeFormation}
                    handleChange={this.handleFormationChange}
                />
                <div className="layout__center layout__full-height">
                    <div className="field">
                        {this.state.playerRows.map((playerRow, row) => {
                            return (
                                // @TODO: should use unique identifiers for rows and players individually
                                // -> use keyed objects (... or Maps hehe) ?
                                <div className="field-row" key={`${row}${row + playerRow.length}`}>
                                    {playerRow.map((player, column) => (
                                        <Jersey
                                            key={`${row}${column}`}
                                            player={player}
                                            row={row}
                                            column={column}
                                            handlePlayerClick={this.handlePlayerClick}
                                        />
                                    ))}
                                </div>
                            );
                        })}
                        <div className="field__reset">
                            <button className="btn btn--flat" onClick={this.handleReset}>
                                Reset
                            </button>
                        </div>
                    </div>
                </div>
                {this.state.modalPlayer.show && (
                    <div
                        className="overlay"
                        onClick={() => this.setState(() => ({ modalPlayer: { show: false } }))}
                    >
                        <Modal title={this.state.modalPlayer.position}>
                            {this.props.players[this.state.modalPlayer.position].map((player) => {
                                const currentRow = this.state.playerRows[this.state.modalPlayer.row];
                                let fadePlayer = false;
                                for (let i = 0; i < currentRow.length; i++) {
                                    if (currentRow[i] === player) {
                                        fadePlayer = true;
                                        break;
                                    }
                                }
                                return (
                                    <button
                                        key={player}
                                        className={`btn btn--underline btn--list ${fadePlayer && 'btn--faden'}`}
                                        onClick={() => this.handlePlayerChange(player)}
                                    >
                                        {player}
                                    </button>
                                );
                            })}
                        </Modal>
                    </div>
                )}
            </div>
        );
    }
}

export default Field;
