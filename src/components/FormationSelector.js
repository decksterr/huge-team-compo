import React, { Component } from 'react';
import { PropTypes } from 'prop-types';

class FormationSelector extends Component {
    static propTypes = {
        formations: PropTypes.array.isRequired,
        activeFormation: PropTypes.string.isRequired,
        handleChange: PropTypes.func.isRequired
    };

    state = {
        isActive: false
    };

    render() {
        return (
            <div>
                {this.state.isActive ? (
                    <div
                        className="overlay"
                        onClick={() => {
                            this.setState(() => ({ isActive: false }));
                        }}
                    >
                        <div className="layout__top-right">
                            {this.props.formations.map((formation) => (
                                <button
                                    key={formation}
                                    className={`btn btn--flat btn--list ${formation ===
                                        this.props.activeFormation && 'active'}`}
                                    onClick={() => {
                                        this.setState(() => ({ isActive: false }));
                                        this.props.handleChange(formation);
                                    }}
                                >
                                    {formation}
                                </button>
                            ))}
                        </div>
                    </div>
                ) : (
                    <div className="layout__top-right">
                        <button
                            className="btn btn--flat"
                            onClick={() => {
                                this.setState(() => ({ isActive: true }));
                            }}
                        >
                            {this.props.activeFormation}
                        </button>
                    </div>
                )}
            </div>
        );
    }
}

export default FormationSelector;
