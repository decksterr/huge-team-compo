import React from 'react';
import ReactDOM from 'react-dom';
import 'normalize.css';
import './styles/main.css';
import MainPage from './containers/MainPage';
import Field from './components/Field';
import registerServiceWorker from './registerServiceWorker';

import players from './assets/data/players_co.json';
import formations from './assets/data/formations_co.json';

ReactDOM.render(
    <MainPage>
        <Field players={players} formations={formations} />
    </MainPage>,
    document.getElementById('root')
);
registerServiceWorker();
