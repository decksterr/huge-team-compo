import React from 'react';
import imgFifaLogo from '../assets/img/icon.png';

const MainPage = (props) => (
    <div className="main-page">
        <img alt="Fifa logo" src={imgFifaLogo} className="layout__top-left" />
        {props.children}
    </div>
);

export default MainPage;
