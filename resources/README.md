# React HomeTest

### Overview

This exercise will have the candidate build a React Single Page Application.

Here are the guidelines for this exercise:

* Chrome compliance is all that's required, all functions and features available in Chrome are in play.
* Code must run after the one `npm` command, please ensure your code runs as you expect it to from a fresh checkout with these commands before submission.

Comps are made for 768 x 1024, responsivenees will not be fully tested, but make sure is not broken for larger sizes.


### Criteria of evaluation

Keep in mind, you should follow the latest best practices of FrontEnd development. We will rate your app in the following areas:

* Scalability
* Project Structure / data Structure / Setup / Environment
* HTML/JSX/CSS/JS quality
* Functionality
* Pixel perfect comps matching
* Documentation 

### Tech details

* Starter Kits are valid.
* You can use whatever CSS solution you want (even pure css), but scalabilty and adherence to React ecosystem are nice to haves (CSS-in-JS, styled-components, etc).
* Using State Managers is a plus (Redux, Flux, Mobx, etc).
* No Server Side Rendering.
* Production bundle is not mandatory, we can test a `dev` version in our local machine, just make sure all is working with a single NPM command.
* You must use the data from `/data/players.txt` and `/data/formations.txt`. Will give you information about the Positions.

### Interactions

###### Field

* Field will show the list of Players ordered by current Formation.
* On click over each Player (shirt) must open the Modal.

###### Modal

* If modal open, name of the Position is shown as a title.
* If modal open, filtered Players list is shown by the position of the Player the user clicked.
* On click in Players list element will close the Modal, then update the position of the player and its availability.
* On click in translucent mask will close the Modal.

###### Select

* On click in Select will show the list of Formations.
* On click of Formations element, will close the Select and will update the distribution of Players in Field.
* On click in translucent mask will close the Select.

###### Reset Button

* On click, Field players will be setted as default and all Players with empty state.

